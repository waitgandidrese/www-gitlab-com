!function(){
  //
  var targetBodyClass = 'egg';
  var appendToLocation = document.querySelector('#main-nav');
  //https://codepen.io/tobyj/pen/QjvEex
  var appendThisContent = '<ul class="lightrope"><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li></ul>';
  //
  appendToLocation.innerHTML += appendThisContent;
  //
  document.addEventListener('keydown', function(event){
    if (event.ctrlKey && event.key === '`') {
      event.preventDefault();
      if (document.body.classList.contains(targetBodyClass))
      {
        document.body.classList.remove(targetBodyClass);
      } else {
        document.body.classList.add(targetBodyClass);
      }
    }
  });
  //
}();